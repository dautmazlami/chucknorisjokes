package com.example.chucknorrisjokes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.chucknorrisjokes.R;
import com.example.chucknorrisjokes.model.Joke;
import com.example.chucknorrisjokes.model.JokeResponseModel;

import java.util.ArrayList;
import java.util.List;

public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.jokeViewHolder>{

    Context context;
    List<Joke> jokes;
    LayoutInflater inflater;

    public JokeAdapter(Context context,List<Joke> jokes) {
        this.context = context;
        this.jokes = jokes;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public jokeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view  = inflater.inflate(R.layout.jokes_item,parent,false);
        return new jokeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull jokeViewHolder holder, int position) {
        Joke joke = jokes.get(position);
        holder.jokeTextView.setText(joke.getJoke());
        holder.categoryTextView.setText(joke.getCategories().toString());
        holder.jokeNumber.setText(joke.getId());
    }

    @Override
    public int getItemCount() {
        return jokes.size();
    }

    public class jokeViewHolder extends RecyclerView.ViewHolder{

        TextView jokeTextView;
        TextView categoryTextView;
        TextView jokeNumber;


        public jokeViewHolder(@NonNull View itemView) {
            super(itemView);
            jokeTextView = itemView.findViewById(R.id.joke_text);
            categoryTextView = itemView.findViewById(R.id.category_kind);
            jokeNumber = itemView.findViewById(R.id.joke_number);

        }
    }
}
