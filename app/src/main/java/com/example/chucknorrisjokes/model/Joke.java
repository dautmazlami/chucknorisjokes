package com.example.chucknorrisjokes.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Joke implements Parcelable {
    String id,joke;
    String[] categories;

    public Joke(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }

    public String[] getCategories() {
        return categories;
    }

    public void setCategories(String[] categories) {
        this.categories = categories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
