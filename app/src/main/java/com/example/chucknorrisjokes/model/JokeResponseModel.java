package com.example.chucknorrisjokes.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class JokeResponseModel implements Parcelable {
    String type;
    List<Joke> value;

    public JokeResponseModel(){}

    protected JokeResponseModel(Parcel in) {
        type = in.readString();
    }

    public static final Creator<JokeResponseModel> CREATOR = new Creator<JokeResponseModel>() {
        @Override
        public JokeResponseModel createFromParcel(Parcel in) {
            return new JokeResponseModel(in);
        }

        @Override
        public JokeResponseModel[] newArray(int size) {
            return new JokeResponseModel[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Joke> getValue() {
        return value;
    }

    public void setValue(List<Joke> value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(type);
    }
}
