package com.example.chucknorrisjokes;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.chucknorrisjokes.adapter.JokeAdapter;
import com.example.chucknorrisjokes.model.Joke;
import com.example.chucknorrisjokes.model.JokeResponseModel;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = "Details";

    RecyclerView recyclerView;
    JokeAdapter adapter;
    List<Joke> jokeList = new ArrayList<>();

    Gson gson;
    String query = "";
    TextView titleTextView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleTextView = findViewById(R.id.app_tittle);
        progressBar = findViewById(R.id.progressbar);

        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL , false));
        adapter = new JokeAdapter(this,jokeList);
        recyclerView.setAdapter(adapter);

        gson = new Gson();
        loadJokes(query);

    }

    void loadJokes(final String query){
        HttpUrl.Builder urlBuilder = HttpUrl.parse("https://api.icndb.com/jokes/random/20").newBuilder();
        urlBuilder.addQueryParameter("value", query);
        String url = urlBuilder.build().toString();

        OkHttpClient client = new OkHttpClient();

        final Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.d(TAG,"error");
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                if (response.isSuccessful()){
                    String jsonString = response.body().string();
                    final JokeResponseModel jokeResponseModel = gson.fromJson(jsonString,JokeResponseModel.class);
                    System.out.println(jokeResponseModel.getType());
                    jokeList = jokeResponseModel.getValue();

                    //Type jokeType = new TypeToken<ArrayList<Joke>>(){}.getType();
                    //final List<Joke> joke = gson.fromJson(jsonString, jokeType);

                    Log.d(TAG,"error");
                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new JokeAdapter(MainActivity.this, jokeList);
                            recyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                            progressBar.setVisibility(View.VISIBLE);
                        }
                    });

                }
            }
        });

    }
}
